(function () {
  const modal = document.querySelector('#skate-app--modal');

  // Event listeners
  document.querySelector('#close-modal--btn').addEventListener('click', closeModal);
  document.querySelector('#goToHome').addEventListener('click', goToHome);
  Array.from(document.querySelectorAll('.goToProducts')).forEach(l => l.addEventListener('click', goToProducts));
  Array.from(document.querySelectorAll('.goToBlog')).forEach(l => l.addEventListener('click', goToBlog));
  Array.from(document.querySelectorAll('.goToGallery')).forEach(l => l.addEventListener('click', goToGallery));

  // Observer
  const callback = (entries) => {
    entries.forEach(entry => {
      entry.target.classList.toggle('show-on-visible');
    });
  };

  const observer = new IntersectionObserver(callback);
  const targets = document.querySelectorAll('.option-entry');
  targets.forEach((target) => {
    observer.observe(target);
  });

  // Close modal when it is clicked outside
  window.onclick = (event) => {
    closeModal(event);
  }

  // Link handlers
  function goToHome() {
    console.log('Going to home');
    navigate('home');
  }

  function goToProducts() {
    console.log('Going to products');
    navigate('home');
  }

  function goToBlog() {
    console.log('Going to blog');
    navigate('blog');
  }

  function goToGallery() {
    console.log('Going to gallery');
    navigate('gallery');
  }

  function openModal() {
    modal.style.display = 'flex';
    // document.querySelector('#skate-app--modal > .modal-content').classList.add('show-modal');
  }

  function closeModal(event) {
    modal.style.display = 'none';
  }

  // Content fetcher
  function navigate(section) {
    const rootDiv = document.getElementById('skate-app--main-content');
    fetch(`/partials/${section}.html`)
      .then(response => {
        return (response.ok) ? response.text() : Promise.reject();
      })
      .then(fragment => rootDiv.innerHTML = fragment)
      .catch(error => {
        openModal();
      });
  }

  // Initial load
  navigate('home');
})(window);